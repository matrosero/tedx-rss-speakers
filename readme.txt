=== TEDx RSS Speakers Feed ===
Contributors: Mat Rosero
Tags: rss, shortcode
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Usage:
[rss-speakers evento="tedxpvsalon-innovacion-2021" max=3]

