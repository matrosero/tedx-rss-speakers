<?php
/**
 * Plugin Name:     TEDx RSS Speakers Feed
 * Plugin URI:      https://gitlab.com/matrosero/tedxpuravida2019
 * Description:     Show speakers from TEDxPuraVida.org using an RSS feed via shortcode
 * Author:          Mat Rosero
 * Author URI:      https://matilderosero.com
 * Text Domain:     tedx-rss-speakers
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Tedx_Rss_Speakers
 */

define( 'TEDX_RSS_SPEAKERS_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'TEDX_RSS_SPEAKERS_PLUGIN_URL', plugin_dir_url(__FILE__) ); 

function tedx_rss_enqueue_styles() {
    
    wp_enqueue_style( 'tedx-rss-syle', TEDX_RSS_SPEAKERS_PLUGIN_URL . 'assets/style/style.css', array(), '', 'all' );
}
add_action( 'wp_enqueue_scripts', 'tedx_rss_enqueue_styles' );

/**
 * Register all shortcodes
 *
 * @return null
 */
function tedx_rss_speakers_register_shortcodes() {
	add_shortcode('rss-speakers', 'tedx_rss_speakers_shortcode');
}
add_action( 'init', 'tedx_rss_speakers_register_shortcodes' );

/*
 * RSS Speakers callback
 * - [rss-speakers]
 *
 * Returns RSS with speakers for specified event
 */
function tedx_rss_speakers_shortcode($atts, $content = null) {
    global $wp_query,
        $post;

	// Attributes
	$atts = shortcode_atts(
		array(
			'evento' => '',
			'max' => 5,
			'type'	=> 'speakers',
			'talk-themes'	=> 0
		),
		$atts
	);

    if ( !$atts['evento'] ) 
        return false;

	$event = sanitize_text_field($atts['evento']);
	$max = sanitize_text_field($atts['max']);

	$type_temp = sanitize_text_field($atts['type']);

	if (in_array($type_temp,array('presenters','speakers'))) {
		$type = $type_temp;
	} else {
		$type = 'speakers';
	}

	$feed = 'https://www.tedxpuravida.org/feed/'.$type.'?evento='.$event;
    

	ob_start();

	// var_dump($atts['talk-themes']);

	include_once( ABSPATH . WPINC . '/feed.php' );
 
	// Get a SimplePie feed object from the specified feed source.
	$rss = fetch_feed( $feed );

	$maxitems = 0;
 
	if ( ! is_wp_error( $rss ) ) : // Checks that the object is created correctly
	
		// Figure out how many total items there are, but limit it to 5. 
		$maxitems = $rss->get_item_quantity( $max ); 
	
		// Build an array of all the items, starting with element 0 (first element).
		$rss_items = $rss->get_items( 0, $maxitems );
	
	endif;
	?>
	<ul class="tedx-speakers-feed speakers">
		<?php if ( $maxitems == 0 ) : ?>
			<li><?php _e( 'No items', 'wpdocs_textdomain' ); ?></li>
		<?php else : ?>
			<?php // Loop through each feed item and display each item as a hyperlink. ?>
			<?php foreach ( $rss_items as $item ) : ?>

				<?php 
				$desc = $item->get_description(); 

				// var_dump($desc);
				// var_dump($atts['talk-themes']);

				preg_match('/<p class="profile-pic">(.*?)<\/p>/s', $desc, $match);

				if ($match[1]) {
					$image = $match[1];
				}

				

				preg_match('/<p class="occupation">(.*?)<\/p>/s', $desc, $match);

				if ($match[1]) {
					$occupation = $match[1];
				}



				preg_match('/<p class="current-role">(.*?)<\/p>/s', $desc, $match);

				if (isset($match[1])) {
					$current_role = $match[1];
				}


				if ($atts['talk-themes'] == 1) {
					preg_match('/<p class="speaker-talk-theme">(.*?)<\/p>/s', $desc, $match);

					if (isset($match[1])) {
						$talk_theme = $match[1];
					}					
				}

				$class = 'speaker';
				if ( isset($current_role) && !empty($current_role) ) { 
					$class .= ' has-current-role'; 
				}
				if ($atts['talk-themes'] == 1) {
					$class .= ' speaker-horizontal';
				}

				?>

				<li class="<?php echo $class; ?>">
					
					
					
					<a class="speaker-link" href="<?php echo esc_url( $item->get_permalink() ); ?>"
						title="<?php printf( __( 'Posted %s', 'wpdocs_textdomain' ), $item->get_date('j F Y | g:i a') ); ?>">

						<div class="speaker-photo"><?php echo $image; ?></div>
						<h5 class="speaker-name"><?php echo esc_html( $item->get_title() ); ?></h5>
						
					

					
						<?php
						if (isset($current_role)) {
							echo '<span class="speaker-occupation speaker-current-role">'.$current_role.'</span>';
						}
						?>
						<span class="speaker-occupation"><?php echo $occupation; ?></span>

						<?php
						if ($atts['talk-themes'] == 1 && isset($talk_theme)) { ?>
							<p class="speaker-talk-theme"><strong>Tema:</strong> <?php echo $talk_theme; ?></p>
						<?php }
						?>
					</a>
				</li>
			<?php endforeach; ?>
		<?php endif; ?>
	</ul>

	<?php
	$output = ob_get_clean();

    return $output;
}